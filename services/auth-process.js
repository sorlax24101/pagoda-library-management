const {BrowserWindow} = require('electron');
const authService = require('./auth-service');
const createAppWindow = require('../app-process');

let win = null;

function createAuthWindow() {
    destroyAuthWin();

    // Create the browser window.
    win = new BrowserWindow({
        width: 1000,
        height: 600,
    });

    win.loadURL(authService.getAuthenticationURL());
    console.log(`URL loaded !!!`);

    const {session: {webRequest}} = win.webContents;

    const filter = {
        urls: [
            // 'http://localhost:3000'
            'file:///callback*'
        ]
    };

    console.log(`filter created`);
    webRequest.onBeforeRequest(filter, async ({url}) => {
        console.log(`Loading tokens`);
        await authService.loadTokens(url);
        console.log(`Tokens loaded. Creating App Window`);
        createAppWindow();
        console.log(`App Window created`);
        return destroyAuthWin();
    });

    win.on('authenticated', () => {
        destroyAuthWin();
    });

    win.on('closed', () => {
        win = null;
    });
}

function destroyAuthWin() {
    if (!win) return;
    win.close();
    win = null;
}

function createLogoutWindow() {
    return new Promise(resolve => {
        const logoutWindow = new BrowserWindow({
            show: false,
        });

        logoutWindow.loadURL(authService.getLogOutUrl());

        logoutWindow.on('ready-to-show', async () => {
            logoutWindow.close();
            await authService.logout();
            resolve();
        });
    });
}

module.exports = {
    createAuthWindow,
    createLogoutWindow,
};