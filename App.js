// const electron = require("electron");
// const {app, BrowserWindow} = require('electron');
// const {remote} = require('electron');
// const remote = electron.remote;
// const axios = require('axios');
// const authService = remote.require('./services/auth-service');
// const authProcess = remote.require('./services/auth-process');
// const authService = require('./services/auth-service');
// const authProcess = require('./services/auth-process');
const {app} = require('electron');

/*
const {createAuthWindow} = require('./services/auth-process');
const createAppWindow = require('./app-process');
const authService = require('./services/auth-service');

async function showWindow() {
    try {
        console.log(`Creating app window`);
        await authService.refreshTokens();
        return createAppWindow();
    } catch (err) {
        // console.log(error);
        console.log(`Creating Authentication window`);
        createAuthWindow();
    }
}
*/

function createWindow (){
    const {BrowserWindow} = require('electron');
    let window = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true
        }
    });
    window.loadFile("./renderers/sign-in.html");

    // Emitted when the window is closed.
    window.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        window = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);
// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') app.quit()
});

app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) createWindow()
});
