const connect = require('express');
const http = require('http');
const net = require('net');

const app = connect();

const requestIp = require('request-ip');
app.use(requestIp.mw());

app.use(function(req, res, next) {
    // require request-ip and register it as middleware
    const requestIp = require('request-ip');

    // you can override which attirbute the ip will be set on by
    // passing in an options object with an attributeName
    app.use(requestIp.mw({ attributeName : 'myCustomAttributeName' }));
    // use our custom attributeName that we registered in the middleware
    const ip = req.myCustomAttributeName;
    console.log(ip);

    // https://nodejs.org/api/net.html#net_net_isip_input
    const ipType = net.isIP(ip); // returns 0 for invalid, 4 for IPv4, and 6 for IPv6
    res.end('Hello, your ip address is ' + ip + ' and is of type IPv' + ipType + '\n');
    next();
});

app.use(function(req, res, next) {
    const clientIp = requestIp.getClientIp(req);
    const ip = req.clientIp;
    console.log("ip: ");
    console.log(ip);
    console.log("\nClient ip: ");
    console.log((clientIp));
    res.end(ip);
    console.log("\nRemote ip: ");
    console.log(req.connection.remoteAddress);
    const getip = require('ipware')().get_ip;
    console.log("\nipware: ");
    console.log(getip(req));
    next();
});


let server = net.createServer(function(socket) {

    socket.on('connect', function() {
        console.log('Client connected: ' + socket.remoteAddress);
        socket.write('Welcome to the server!\r\n');
    });

    socket.on('end', function() {
        console.log('Client disconnected: ' + socket.remoteAddress);
    });

    socket.on('error', function() {
        console.log('Client error: ' + socket.remoteAddress);
    });

    socket.on('timeout', function() {
        console.log('Client timed out:' + socket.remoteAddress);
    });


}).listen(6346);

// http.createServer(app).listen(8081);
// console.log(http.socket.remoteAddress);

console.log("Sign in file loaded");


document.getElementById('sign-in-button').onclick = () => {
    console.log("Sign in button clicked!!!");
};
