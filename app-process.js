const {BrowserWindow} = require('electron');

function createAppWindow() {
    let win = new BrowserWindow({
        width: 1000,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        },
    });
    console.log(`Window created. Looking for dashboard`);

    win.loadFile('./renderers/dashboard.html');
    console.log(`Dashboard loaded`);

    win.on('closed', () => {
        win = null;
    });
}

module.exports = createAppWindow;